![Folders2Web](name.png "Folders2Web")

Folders2Web
===========

    Author: Ray Luo <rayluo.mba@gmail.com>
    Date: 2016-07

![Icon](logo.png)

Folders2Web allows you to organize your notes or posts by folders,
in the same way as how you organize your files in your local hard disk.
You can then store them in VCS, and publish them as a static website.

The workflow of using Folders2Web looks like this:

1. Write your notes or blog posts in
   [Markdown](https://en.wikipedia.org/wiki/Markdown#Example)
2. Group a post and its pictures in their own folder,
   so that different posts are natually separated in different folders.
   Besides, posts can reference each other by linking to their `../sibling/file.md`
3. Run a `build.py` script
   (you may need to install [Python](http://python.org/) first).
   Then your posts will be built into a lightweight manifest file `_posts.json`,
   which will later be rendered into a website at the client side.
4. Host your manifest file and all posts directly on Github or Bitbucket
   (it is free), or host them on any static web hosting service.

