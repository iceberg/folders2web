#!`which python`
import os, json
import re
import doctest


def parse(fileobj, info):
    """
    Parse a markdown file and grab its title, date, etc.
    >>> result = parse(None, {})
    >>> result['title']
    'Header'
    >>> result['date']
    '2016-07-28'
    """
    testcase = '''
Header
===

    Date: 2016-07-28
'''
    if fileobj is None:
        payload = testcase
    else:
        payload = fileobj.read()  # We assume the file is not very big
    underlined_header = re.search(r"\s*(.+)(?:\n|\r\n?)[=-]{3,}", payload)
    info['title'] = underlined_header.group(1) if underlined_header else None
    datestamp = re.search(r"\s*Date: (.*)", payload)
    info['date'] = datestamp.group(1) if datestamp else None
    return info

def openfile(filepath):
    try:
        return open(filepath, encoding="utf-8")  # Python 3
    except:
        return open(filepath)  # Python 2

doctest.testmod()
posts = []
for root, dirs, files in os.walk('.'):
    dirs.sort(); files.sort()  # Only works when not os.walk(..., topdown=False)
    for f in files:
        if f.endswith('.md'):
            post = os.path.join(os.path.normpath(root), f)
            print("Processing {} ...".format(post))
            posts.append(parse(openfile(post), {'path': post.replace(os.sep, '/')}))
json.dump(
    posts, open('_posts.json', 'w'),
    sort_keys=True, indent=4, separators=(',', ': '))
print("_posts.json is successfully created. "
    "You can run this to preview your website: python -m SimpleHTTPServer")

