Powered by Folders2Web
======================

This repo is organized in a structure defined by Folders2Web,
so that the [Markdown](https://en.wikipedia.org/wiki/Markdown#Example) files
as well as their corresponding pictures can be grouped by different folders,
and then be converted into a static website.

See [Folders2Web description](folders2web/about.md) for more details.

